const mongoose = require('mongoose');

const noteSchema = mongoose.Schema(
  {
    userId: { type: String, required: true },
    completed: { type: Boolean, default: false, required: false },
    text: { type: String, required: true },
  },
  { timestamps: true },
);

module.exports = mongoose.model('Note', noteSchema);
