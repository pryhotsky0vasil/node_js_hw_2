const express = require('express');
const authMiddleware = require('../middlewares/authMiddleware');
const { getUserInfo, deleteUser, changeUserPassword } = require('../services/usersService');

const router = express.Router();

router.get('/me', getUserInfo);
router.delete('/me', deleteUser);
router.patch('/me', authMiddleware, changeUserPassword);

module.exports = router;
