const express = require('express');
const {
  checkNote,
  createNote,
  deleteNote,
  editNote,
  getNote,
  getNotes,
} = require('../services/notesService');

const router = express.Router();

router.get('/', getNotes);
router.post('/', createNote);
router.get('/:_id', getNote);
router.put('/:_id', editNote);
router.patch('/:_id', checkNote);
router.delete('/:_id', deleteNote);

module.exports = router;
