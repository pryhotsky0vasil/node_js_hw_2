/* eslint-disable linebreak-style */
const express = require('express');

const router = express.Router();

const authRouter = require('./authRouter');
const userRouter = require('./usersRouter');
const notesRouter = require('./notesRouter');
const authMiddleware = require('../middlewares/authMiddleware');

router.use('/auth', authRouter);
router.use('/users', authMiddleware, userRouter);
router.use('/notes', authMiddleware, notesRouter);

module.exports = router;
