const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {
  try {
    const [, token] = req.headers.authorization.split(' ');

    if (!token) {
      return res.status(400).send({ message: 'Please, include token to request' });
    }

    const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);

    req.user = tokenPayload;

    next();
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }
};

module.exports = auth;
