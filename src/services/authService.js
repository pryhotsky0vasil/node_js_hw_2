const bcrypt = require('bcryptjs');

const User = require('../models/User');
const generateJWT = require('./JWTService');

const createUser = async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const createdUser = new User({
      username,
      password: await bcrypt.hash(password, 10),
    });

    await createdUser.save();
    return res.status(200).send({ message: 'User successfully registered' });
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }
};

const login = async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ username });

    if (user && (await bcrypt.compare(String(password), String(user.password)))) {
      const token = generateJWT(user);

      return res.status(200).send({ message: 'User successful authorized', jwt_token: token });
    }
    return res.status(400).send({ message: 'Not authorized' });
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }
};

module.exports = {
  createUser,
  login,
};
