/* eslint-disable linebreak-style */
const jwt = require('jsonwebtoken');

const generateJWT = ({ username, id }) => jwt.sign({ username, id }, process.env.SECRET_KEY);

module.exports = generateJWT;
