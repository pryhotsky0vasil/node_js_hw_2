const Note = require('../models/Note');

const getNotes = async (req, res, next) => {
  try {
    const {
      user: { id: userId },
      query: { offset = 0, limit = 0 },
    } = req;

    const notes = await Note.find({ userId }).skip(offset).limit(limit);

    return res.status(200).send({
      offset,
      limit,
      count: notes.length,
      notes,
    });
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }
};

const getNote = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const note = await Note.findById(_id);

    return res.status(200).send({ note });
  } catch (err) {
    return res.status(400).send({
      message: err.message,
    });
  }
};

const createNote = async (req, res, next) => {
  try {
    const {
      user: { id: userId },
      body: { text },
    } = req;

    const note = new Note({
      text,
      userId,
    });

    await note.save();

    return res.status(200).send({ note, message: 'Note successfully created' });
  } catch (err) {
    return res.status(400).send({
      message: err.message,
    });
  }
};

const editNote = async (req, res, next) => {
  try {
    const {
      params: { _id },
      body: { text },
    } = req;

    await Note.findByIdAndUpdate(_id, { text });

    return res.status(200).send({
      message: 'Note successfully edited',
    });
  } catch (err) {
    return res.status(400).send({
      message: err.message,
    });
  }
};

const checkNote = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const note = await Note.findById(_id);

    await Note.findByIdAndUpdate({ _id }, { completed: !note.completed });

    return res.status(200).send({ message: 'Note successfully checked' });
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }
};

const deleteNote = async (req, res, next) => {
  try {
    const { _id } = req.params;

    await Note.findByIdAndDelete({ _id });

    return res.status(200).send({ message: 'Note successfully deleted' });
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }
};

module.exports = {
  getNotes,
  createNote,
  getNote,
  editNote,
  checkNote,
  deleteNote,
};
