const bcrypt = require('bcryptjs');

const User = require('../models/User');

const getUserInfo = async (req, res, next) => {
  try {
    const { _id, username, createdAt } = await User.findById(req.user.id);

    return res.status(200).send({
      user: {
        _id,
        username,
        createdAt,
      },
    });
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const { id } = req.user;

    await User.findByIdAndDelete(id);

    return res.status(200).send({ message: 'User successfully deleted' });
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }
};

const changeUserPassword = async (req, res, next) => {
  try {
    const {
      user: { id },
      body: { oldPassword, newPassword },
    } = req;
    const user = await User.findById(id);

    if (!(await bcrypt.compare(String(oldPassword), String(user.password)))) {
      throw new Error('Password is wrong');
    }

    user.password = await bcrypt.hash(newPassword, 10);

    await user.save();

    return res.status(200).send({ message: 'Password successfully changed' });
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }
};

module.exports = {
  getUserInfo,
  deleteUser,
  changeUserPassword,
};
