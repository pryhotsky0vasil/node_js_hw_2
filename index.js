const express = require('express');
const cors = require('cors');
const { connect } = require('mongoose');
const morgan = require('morgan');

require('dotenv').config();

const appRouters = require('./src/routers/index');

const {PORT = 8080, DB_USER, DB_PASSWORD } = process.env;
const URI = `mongodb+srv://${DB_USER}:${DB_PASSWORD}@cluster0.xgm48oz.mongodb.net/?retryWrites=true&w=majority`;

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api', appRouters);

const start = async () => {
  try {
    await connect(URI);

    app.listen(PORT, () => {
      console.log(`Server has been started on port ${PORT}`);
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
